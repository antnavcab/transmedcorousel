/**
 * Automatically generated file. DO NOT MODIFY
 */
package p3x.carousel.transmedcarousel.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "p3x.carousel.transmedcarousel.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.0";
}
