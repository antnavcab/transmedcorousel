package p3x.caorusel.transmedcarousel;

import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.view.Menu;
import android.view.MenuItem;


import p3x.carousel.transmedcarousel.R;

public class MainActivity extends AppCompatActivity {

    // Atributos de la clase
    // imagen --> objeto usado para acceder a la imagen que mostramos al usuario
    public ImageView imagen;
    // Listado de imagenes (hay que inicializarlo con los recursos que tengamos)
    public int[] recursosImagenes = new int[10];
    // Listado de sonidos (hay que inicializarlo con los recursos que tengamos)
    // mucho ojo, usamos el mismo índice en estos dos listados para poner una imagen y su sonido
    public int[] sonidosAnimales = new int[10];
    // puntero que usaremos para recorrer el listado de imagenes
    public int puntero = 1;

    /*Clases necesarios para aplicar efectos de sonidos*/
    private SoundPool soundPool;
    AudioManager audioManager;
    boolean plays = false, loaded = false;
    float actVolume, maxVolume, volume;
    int idAnimalSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Rellenamos los arrays de las imagenes y de los sonidos
        this.inicializarRecursos();

        // Enlazamos el objeto imagen a la imagen que hemos definido en el layout
        this.imagen = (ImageView) findViewById(R.id.imagen);

        // asignamos un valor aleatorio al puntero
        this.puntero = (int) (Math.random() * 10);
        // Mostramos en la imagen el recurso que tenemos apuntado por el puntero
        this.imagen.setImageResource(recursosImagenes[puntero]);
        // hacemos sonar el sonido correspondiente
        this.soundPool.play(sonidosAnimales[puntero],1,1,0,0,1);
        // preparamos el puntero para mostrar ya la siguiente imagen
        this.puntero++;
        // Añadimos un clickListener a la imagen para que ejecute una acción cuando el
        // usario la pulse
        this.imagen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Para cada click mostramos la imagen que tenemos apuntada por el puntero y
                // hacemos sonar el sonido correspondiente
                //makeText(getApplicationContext(), "Imagen pulsada", LENGTH_SHORT).show();
                imagen.setImageResource(recursosImagenes[puntero]);
                soundPool.play(sonidosAnimales[puntero],1,1,0,0,1);
                // preparamos el puntero para la siguiente imagen
                puntero++;
                // si nos salimos de la lista volvemos a empezar
                if(puntero>9) puntero = 0;
            }

        });
    }

    /** sonidos
     * Metodo de carga de sonidos
     */
    private void inicializarRecursos() {

        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC,0);

        this.recursosImagenes[0] = R.drawable.caballo;
        this.recursosImagenes[1] = R.drawable.cerdo;
        this.recursosImagenes[2] = R.drawable.elefante;
        this.recursosImagenes[3] = R.drawable.vaca;
        this.recursosImagenes[4] = R.drawable.oveja;
        this.recursosImagenes[5] = R.drawable.leon;
        this.recursosImagenes[6] = R.drawable.mono;
        this.recursosImagenes[7] = R.drawable.perro;
        this.recursosImagenes[8] = R.drawable.gato;
        this.recursosImagenes[9] = R.drawable.gallo;

        this.sonidosAnimales[0] = soundPool.load(getApplicationContext(),R.raw.avion,1);
        this.sonidosAnimales[1] = soundPool.load(getApplicationContext(),R.raw.barco,1);
        this.sonidosAnimales[2] = soundPool.load(getApplicationContext(),R.raw.bicicleta,1);
        this.sonidosAnimales[3] = soundPool.load(getApplicationContext(),R.raw.coche,1);
        this.sonidosAnimales[4] = soundPool.load(getApplicationContext(),R.raw.avioneta,1);
        this.sonidosAnimales[5] = soundPool.load(getApplicationContext(),R.raw.helicoptero,1);
        this.sonidosAnimales[6] = soundPool.load(getApplicationContext(),R.raw.submarino,1);
        this.sonidosAnimales[7] = soundPool.load(getApplicationContext(),R.raw.tren,1);
        this.sonidosAnimales[8] = soundPool.load(getApplicationContext(),R.raw.moto,1);
        this.sonidosAnimales[9] = soundPool.load(getApplicationContext(),R.raw.caballo,1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sobre) {
            // TODO: Comprobar si se puede definir el intent fuera del método para que no se cree cada vez que se llama
            // Cargamos la actividad sobre
            Intent intent = new Intent();
            // definimos las propiedades del intent
            intent.setClass(MainActivity.this, AboutActivity.class);
            // lo ejecutamos para pasar a la actividad destino
            startActivity(intent);
            //makeText(getApplicationContext(), "BOTON SOBRE", LENGTH_SHORT).show();
        } else if (id == R.id.action_salir) {
            // Cerramos la aplicación
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
